<?php

/**
 * @author Jean Carlos <jeancsil@gmail.com>
 */
class Number
{
    /**
     * @var int
     */
    private $value;

    /**
     * @param integer $value
     */
    public function __construct($value)
    {
        if (!is_int($value)) {
            throw new \InvalidArgumentException(sprintf('%s is not integer.', $value));
        }

        $this->value = $value;
    }

    /**
     * @param $by
     * @return bool
     */
    public function isMultipleBy($by) {
        return ($this->value % $by) == 0;
    }

    public function __toString()
    {
        return (string) $this->value;
    }
}
