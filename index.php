<?php
require_once 'src/Number.php';

for($iterator = 1; $iterator<=100; $iterator++) {
    $number = new Number($iterator);

    if ($number->isMultipleBy(3) && $number->isMultipleBy(5)) {
        echo $number . ' FizzBuzz' . PHP_EOL;
        continue;
    }

    if ($number->isMultipleBy(3)) {
        echo 'Fizz ' . PHP_EOL;
    } else if ($number->isMultipleBy(5)) {
        echo $number . ' Buzz' . PHP_EOL;
    }
}
